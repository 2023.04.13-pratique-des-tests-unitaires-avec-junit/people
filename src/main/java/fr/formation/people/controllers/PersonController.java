package fr.formation.people.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.people.models.Person;
import fr.formation.people.services.PersonService;

@RestController
@RequestMapping("people")
public class PersonController extends GenericController<Long, Person, PersonService> {

}
