package fr.formation.people.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.people.models.Address;
import fr.formation.people.services.AddressService;

@RestController
@RequestMapping("addresses")
public class AddressController extends GenericController<Long, Address, AddressService> {

}
