package fr.formation.people.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import fr.formation.people.exceptions.BadRequestException;
import fr.formation.people.models.HasId;
import fr.formation.people.services.GenericService;
import jakarta.validation.Valid;

public abstract class GenericController<KEY extends Number, MODEL extends HasId<KEY>, SERVICE extends GenericService<?, MODEL, KEY>> {

	@Autowired
	protected SERVICE service;
	
	@GetMapping
	public Collection<MODEL> findAll() {
		return service.findAll();
	}

	@GetMapping("{id:\\d+}")
	public MODEL findById(@PathVariable KEY id) {
		return service.findById(id);
	}
	
	@PostMapping
	public MODEL save(@Valid @RequestBody MODEL m) {
		if (m.getId().intValue() != 0)
			throw new BadRequestException("a new entity cannot have a non-null id");
		return service.save(m);
	}
	
	@PutMapping("{id:\\d+}")
	public MODEL update(@PathVariable KEY id, @Valid @RequestBody MODEL m) {
		if (m.getId() != id)
			throw new BadRequestException("ids in url and body do no match");
		return service.update(m);
	}
	
	@DeleteMapping("{id:\\d+}")
	public void delete(@PathVariable KEY id) {
		service.deleteById(id);
	}
	
}
