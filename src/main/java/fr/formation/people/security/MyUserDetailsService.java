package fr.formation.people.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.formation.people.models.Person;
import fr.formation.people.services.PersonService;
import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

	private PersonService personService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Person p = personService.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("email " + username + "not found"));
		return new MyUserDetails(p);
	}

}
