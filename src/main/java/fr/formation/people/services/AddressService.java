package fr.formation.people.services;

import org.springframework.stereotype.Service;

import fr.formation.people.models.Address;
import fr.formation.people.repositories.AddressRepository;

@Service
public class AddressService extends GenericService<AddressRepository, Address, Long>{

}
