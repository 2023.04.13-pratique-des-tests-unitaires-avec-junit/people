package fr.formation.people.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.people.exceptions.NotFoundException;
import fr.formation.people.models.HasId;
import jakarta.transaction.Transactional;

public class GenericService<REPOSITORY extends JpaRepository<MODEL, KEY>, MODEL extends HasId<KEY>, KEY extends Number> {

	@Autowired
	protected REPOSITORY repository;
	
	public Collection<MODEL> findAll() {
		return repository.findAll();
	}
	
	public MODEL findById(KEY id) {
		return repository.findById(id).orElseThrow(() -> new NotFoundException("no entity with id " + id + " exists"));
	}
	
	public MODEL save(MODEL m) {
		return repository.save(m);
	}

	@Transactional
	public MODEL update(MODEL m) {
		if (repository.findById(m.getId()).isEmpty())
			throw new NotFoundException("no entity with id " + m.getId() + " exists");
		return repository.save(m);
	}
	
	@Transactional
	public void deleteById(KEY id) {
		repository.findById(id).ifPresentOrElse(
				m -> delete(m), 
				() -> { throw new NotFoundException("no entity with id " + id + " exists"); }
		);
	}
	
	@Transactional
	public void delete(MODEL m) {
		repository.delete(m);
	}
	
}
