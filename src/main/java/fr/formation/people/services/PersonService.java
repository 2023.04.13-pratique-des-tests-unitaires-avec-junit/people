package fr.formation.people.services;

import java.util.Optional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.formation.people.models.Person;
import fr.formation.people.repositories.PersonRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PersonService extends GenericService<PersonRepository, Person, Long>{

	private PasswordEncoder passwordEncoder;
	
	@Override
	public Person save(Person p) {
		p.setPassword(passwordEncoder.encode(p.getPassword()));
		return super.save(p);
	}
	
	public Optional<Person> findByEmail(String email) {
		return repository.findByEmail(email);
	}
}
