package fr.formation.people.models;

public enum PersonRole {
	USER,
	ADMIN
}
