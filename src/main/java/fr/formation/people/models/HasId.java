package fr.formation.people.models;

public interface HasId<T extends Number> {
	T getId();
	void setId(T id);
}
