package fr.formation.people.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
public class Address implements HasId<Long> {


	@EqualsAndHashCode.Include
	@Builder.Default
	@Id
	@GeneratedValue
	private Long id = 0L;
	
	@NotBlank
	private String city;
	
	@ManyToOne
	private Person owner;
}
