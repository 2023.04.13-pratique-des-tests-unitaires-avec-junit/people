package fr.formation.people.models;

import java.util.Set;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
public class Person implements HasId<Long> {

	@EqualsAndHashCode.Include
	@Builder.Default
	@Id
	@GeneratedValue
	private Long id = 0L;
	
	@Email
	private String email;
	
	@NotBlank
	@Length(min = 8)
	private String password;
	
	private PersonRole role;
	
	@OneToMany(mappedBy="owner")
	@JsonIgnore
	private Set<Address> addresses;
}
