package fr.formation.people.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.people.models.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
