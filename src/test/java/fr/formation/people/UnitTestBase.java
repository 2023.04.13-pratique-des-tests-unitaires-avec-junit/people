package fr.formation.people;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;

import fr.formation.people.repositories.AddressRepository;
import fr.formation.people.repositories.PersonRepository;

@SpringBootTest
@Profile("unit-test")
public class UnitTestBase {

	@MockBean
	protected PersonRepository personRepository;
	
	@MockBean
	protected AddressRepository addressRepository;
}
