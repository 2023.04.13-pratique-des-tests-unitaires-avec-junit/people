package fr.formation.people;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class PeopleApplicationTests {

	@Test
	void contextLoads() {
	}

}
