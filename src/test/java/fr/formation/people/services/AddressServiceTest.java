package fr.formation.people.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fr.formation.people.UnitTestBase;
import fr.formation.people.models.Address;


class AddressServiceTest extends UnitTestBase {

	@Autowired
	private AddressService addressService;
	
	
	@BeforeEach
	void setUp() throws Exception {
		reset(addressRepository);
	}

	@Test
	void testFindAllSuccess() {
		// arrange
		var fixtures = List.of(
				Address.builder().id(123L).city("Marseille").build(),
				Address.builder().id(124L).build(),
				Address.builder().id(125L).build());
		when(addressRepository.findAll()).thenReturn(fixtures);
		// act
		var result = addressService.findAll();
		// assert
		assertEquals(fixtures, result);
		verify(addressRepository, times(1)).findAll();
		verifyNoMoreInteractions(addressRepository);
	}

}
