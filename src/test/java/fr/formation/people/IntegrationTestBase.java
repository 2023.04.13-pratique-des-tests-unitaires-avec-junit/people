package fr.formation.people;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@Profile("integration-test")
@AutoConfigureMockMvc
public class IntegrationTestBase {

	@Autowired
	protected ObjectMapper objectMapper;
	
	@Autowired
	protected MockMvc mockMvc;

}
