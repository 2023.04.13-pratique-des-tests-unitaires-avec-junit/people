package fr.formation.people.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.formation.people.IntegrationTestBase;
import fr.formation.people.models.Address;

@AutoConfigureMockMvc
class AddressControllerIT extends IntegrationTestBase {
	
	@Test
	void testFindAll() throws Exception {
		// arrange
		// act
		var result = mockMvc.perform(get("/addresses"));
		// assert
		result
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(4)));
	}
	
	
	@Test
	void testSaveSuccess() throws JsonProcessingException, Exception {
		// arrange
		Address a = Address.builder().city("Marseille").build();
		// act
		var result = mockMvc.perform(post("/addresses")
				.content(objectMapper.writeValueAsBytes(a))
				.contentType(MediaType.APPLICATION_JSON));
		// assert
		result
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.city", is("Marseille")));
	}

	@Test
	void testSaveErrorIdNotZero() throws JsonProcessingException, Exception {
		// arrange
		Address a = Address.builder().id(123L).city("Marseille").build();
		// act
		var result = mockMvc.perform(post("/addresses")
				.content(objectMapper.writeValueAsBytes(a))
				.contentType(MediaType.APPLICATION_JSON));
		// assert
		result
			.andExpect(status().isBadRequest());
	}

}
