package fr.formation.people.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.formation.people.ControllerUnitTestBase;
import fr.formation.people.models.Address;

@AutoConfigureMockMvc
class AddressControllerTest extends ControllerUnitTestBase {
	
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testSaveSuccess() throws JsonProcessingException, Exception {
		// arrange
		Address a = Address.builder().city("Marseille").build();
		when(addressService.save(a)).thenReturn(a);
		// act
		var result = mockMvc.perform(post("/addresses")
				.content(objectMapper.writeValueAsBytes(a))
				.contentType(MediaType.APPLICATION_JSON));
		// assert
		result
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.city", is("Marseille")));
		verify(addressService, times(1)).save(a);
		verifyNoMoreInteractions(addressService);
	}

	@Test
	void testSaveErrorIdNotZero() throws JsonProcessingException, Exception {
		// arrange
		Address a = Address.builder().id(123L).city("Marseille").build();
		// act
		var result = mockMvc.perform(post("/addresses")
				.content(objectMapper.writeValueAsBytes(a))
				.contentType(MediaType.APPLICATION_JSON));
		// assert
		result
			.andExpect(status().isBadRequest());
		verifyNoInteractions(addressService);
	}

}
