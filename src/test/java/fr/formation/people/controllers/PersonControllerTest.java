package fr.formation.people.controllers;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithMockUser;

import fr.formation.people.ControllerUnitTestBase;
import fr.formation.people.exceptions.NotFoundException;

class PersonControllerTest extends ControllerUnitTestBase {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void testDeleteByIdSuccess() throws Exception {
		// arrange
		// act
		var result = mockMvc.perform(delete("/people/123"));
		// assert
		result.andExpect(status().isOk());
		verify(personService).deleteById(123L);
		verifyNoMoreInteractions(personService);
	}

	@Test
	void testDeleteByIdErrorNotAuthenticated() throws Exception {
		// arrange
		// act
		var result = mockMvc.perform(delete("/people/123"));
		// assert
		result.andExpect(status().isUnauthorized());
		verifyNoInteractions(personService);
	}

	@Test
	@WithMockUser(roles = "USER")
	void testDeleteByIdErrorNotAuthorized() throws Exception {
		// arrange
		// act
		var result = mockMvc.perform(delete("/people/123"));
		// assert
		result.andExpect(status().isForbidden());
		verifyNoInteractions(personService);
	}


	@Test
	@WithMockUser(roles = "ADMIN")
	void testDeleteByIdErrorIdDoesNotExist() throws Exception {
		// arrange
		doThrow(new NotFoundException()).when(personService).deleteById(123L);
		// act
		var result = mockMvc.perform(delete("/people/123"));
		// assert
		result.andExpect(status().isNotFound());
		verify(personService).deleteById(123L);
		verifyNoMoreInteractions(personService);
	}

}
