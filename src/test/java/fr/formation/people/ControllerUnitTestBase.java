package fr.formation.people;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.formation.people.services.AddressService;
import fr.formation.people.services.PersonService;

@AutoConfigureMockMvc
public class ControllerUnitTestBase extends UnitTestBase {

	@Autowired
	protected ObjectMapper objectMapper;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	protected PersonService personService;
	
	@MockBean
	protected AddressService addressService;
}
