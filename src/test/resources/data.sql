
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS person;


CREATE TABLE person (
  id bigint NOT NULL,
  email varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  role smallint DEFAULT NULL,
  PRIMARY KEY (id)
) ;

INSERT INTO person VALUES 
	(152,'Jayda.Stoltenberg71@hotmail.co','aaaaaaaa',1),
  (153,'Princess_Tremblay18@gmail.com','7zmmQ_3rd1Cz6_y',1),
  (159,'Roxanne86@hotmail.com','ecD2A5F1JZ5lmTQ',0),
  (160,'Friedrich_Hickle@yahoo.com','rTCxETDO7Fh0HZV',0),
  (202,'aq@a.c','aaaaaaaa',1),
  (252,'b@b.b','bbbbbbbbbbbb',1);



CREATE TABLE address (
  id bigint NOT NULL,
  city varchar(255) DEFAULT NULL,
  owner bigint DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (owner) REFERENCES person(id)
) ;

INSERT INTO address VALUES 
  (1, "Marseille", 152)
  (2, "Aix", 159)
  (3, "Toulon", 152)
  (4, "Apt", 252);
